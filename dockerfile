# syntax=dockerfile
FROM python:3.8

RUN pip install --upgrade pip

RUN mkdir /app

WORKDIR /app

COPY . /app

RUN pip install -r requirements.txt

EXPOSE 8080

ENTRYPOINT [""]

CMD ["app/app.py"]

